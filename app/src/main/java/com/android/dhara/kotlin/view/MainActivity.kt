package com.android.dhara.kotlin.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.android.dhara.kotlin.R
import com.android.dhara.kotlin.model.MainInteractorImpl
import com.android.dhara.kotlin.presenter.MainPresenterImpl

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val view = MainViewImpl(this)
        val interactor = MainInteractorImpl()
        val presenter = MainPresenterImpl(view, interactor)
        presenter.handleOnViewCreated()
    }
}