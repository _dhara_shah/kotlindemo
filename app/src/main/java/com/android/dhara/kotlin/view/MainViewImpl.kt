package com.android.dhara.kotlin.view

import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainViewImpl (private val activity : AppCompatActivity) : MainView {

    override fun initViews(listener: ViewInteractionListener) {
        activity.btn_click_me.setOnClickListener {
            listener.onClick()
        }
    }

    override fun updateView(value: Int) {
        activity.txt_update_text.text = value.toString()
    }
}