package com.android.dhara.kotlin.model

interface MainInteractor {
    fun updateCounter()

    fun getCounter() : Int
}