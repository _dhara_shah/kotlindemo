package com.android.dhara.kotlin.view

interface MainView {
    fun updateView(value : Int)

    fun initViews(listener : ViewInteractionListener)
}