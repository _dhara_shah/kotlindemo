package com.android.dhara.kotlin.model

class MainInteractorImpl : MainInteractor {
    var value : Int = 0

    override fun updateCounter() {
        value++
    }

    override fun getCounter(): Int {
        return value
    }
}