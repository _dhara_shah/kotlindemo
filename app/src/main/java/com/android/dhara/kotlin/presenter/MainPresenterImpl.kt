package com.android.dhara.kotlin.presenter

import com.android.dhara.kotlin.model.MainInteractor
import com.android.dhara.kotlin.view.MainView
import com.android.dhara.kotlin.view.ViewInteractionListener

class MainPresenterImpl(private val view: MainView,
                        private val interactor: MainInteractor) : MainPresenter, ViewInteractionListener {

    override fun handleOnViewCreated() {
        view.initViews(this)
    }

    override fun onClick() {
        interactor.updateCounter()
        view.updateView(interactor.getCounter())
    }
}