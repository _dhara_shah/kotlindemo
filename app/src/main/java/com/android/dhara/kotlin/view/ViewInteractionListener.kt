package com.android.dhara.kotlin.view

interface ViewInteractionListener {
    fun onClick()
}