package com.android.dhara.kotlin.presenter

interface MainPresenter {
    fun handleOnViewCreated()
}